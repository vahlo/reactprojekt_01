import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';


class App extends Component {

constructor(props){
  super(props);
  console.log('constructor')
}

componentWillMount(){
  console.log('Will mount')
}


componentDidMount(){
  console.log('Did mount after render')

}

state = {
  toogle: true
}
toogle = () => {
  this.setState({
    toogle: !this.state.toogle
  })
}

  render() {
    return (
      <div className="App">
      
         <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Welcome  welcomeText ="Hello to props" toogle= {this.state.toogle} />

        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
         
      
        </p>
        {this.state.toogle &&
        <p> This Should show and hide</p>
        }
        
        <button onClick={this.toogle} > Show / Hide </button>
      </div>
    );
  }
}


class Welcome extends Component{
  render(){
    const { welcomeText , toogle} = this.props;
    console.log(toogle)
    return(
      <h1 className="App-title">{welcomeText + ", Toogle state = " +toogle}</h1>
    )
  }
}


export default App;
